package com.demo.devops.leapyearcalculator;

public class FizzBuzzCalculator {

	public String GetString(int i) {
		if (IsDivideBy3(i) && isDivideBy5(i)) {
			return "FizzBuzz";
		}
		else if (IsDivideBy3(i)) {
			return "Fizz";
		} else if (isDivideBy5(i)){
			return "Buzz";
		}else {
		}
			return Integer.toString(i);			
		}
	
	public boolean isDivideBy5(int i) {
		return i % 5 == 0;
	}
	
		public boolean IsDivideBy3(int i) {
			return i % 3 == 0;
		}
	}
