package com.demo.devops.leapyearcalculator;

import junit.framework.TestCase;

public class FizzBuzzCalculatorTest extends TestCase {
	public void testShouldReturn1_When_Given1() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.GetString(1);
		
		// Assert
		assertEquals("1", result);
	}
	
	public void testShouldReturn2_When_Given2() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.GetString(2);
		
		// Assert
		assertEquals("2", result);
	}
	
	public void testShouldReturnFizz_When_Given3() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.GetString(3);
		
		// Assert
		assertEquals("Fizz", result);
	}
	
	public void testShouldReturnBuzz_When_Given5() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.GetString(5);
		
		// Assert
		assertEquals("Buzz", result);
	}
	
	public void testShouldReturnFizzBuzz_When_Given15() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.GetString(15);
		
		// Assert
		assertEquals("FizzBuzz", result);
	}
}
